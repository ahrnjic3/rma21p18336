package ba.etf.rma21.projekat.data.repositories

import ba.etf.rma21.projekat.data.models.Grupa
import ba.etf.rma21.projekat.data.models.Kviz
import java.util.*

fun grupe(): List<Grupa> {
    return listOf(
            Grupa("Grupa1","RMA"),
            Grupa("Grupa2","RMA"),
            Grupa("Grupa1","DM"),
            Grupa("Grupa2","DM"),
            Grupa("Grupa1","IM"),
            Grupa("Grupa2","IM"),
            Grupa("Grupa1","VI"),
            Grupa("Grupa2","VI"),
            Grupa("Grupa1","NASP"),
            Grupa("Grupa2","NASP"),
            Grupa("Grupa1","NOVI"),
            Grupa("Grupa2","NOVI")

    )
}

