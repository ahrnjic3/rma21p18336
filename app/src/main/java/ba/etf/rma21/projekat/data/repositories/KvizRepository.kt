package ba.etf.rma21.projekat.data.repositories

import ba.etf.rma21.projekat.data.models.Korisnik
import ba.etf.rma21.projekat.data.models.Kviz
import java.util.*

class KvizRepository {

    companion object {
        // TODO: Implementirati
        init {
            // TODO: Implementirati
        }

        fun getMyKvizes(): List<Kviz> {

            return Korisnik.kvizovi;
        }

        fun getAll(): List<Kviz> {
            // TODO: Implementirati
            return kvizovi();
        }

        fun getDone(): List<Kviz> {
            // TODO: Implementirati
            return sortiraj(filtriraj(kvizovi().filter { it.osvojeniBodovi!= null }));
        }

        fun getFuture(): List<Kviz> {
            // TODO: Implementirati
            return sortiraj(filtriraj(kvizovi().filter { Calendar.getInstance().time < it.datumPocetka && it.osvojeniBodovi==null }));
        }

        fun getNotTaken(): List<Kviz> {
            // TODO: Implementirati
            return sortiraj(filtriraj(kvizovi().filter { Calendar.getInstance().time > it.datumKraj  && it.osvojeniBodovi==null}))
        }
        fun sortiraj( kvizList : List<Kviz>): List<Kviz>{
            return kvizList.sortedBy { it.datumPocetka }
        }
        fun filtriraj ( kvizList: List<Kviz>) :List<Kviz>{
            val lista = ArrayList<Kviz>()
            kvizList.forEach {
                if(Korisnik.kvizovi.contains(it)) lista.add(it)
            }
            return lista;
        }
        fun dodajUMojeKvizove( predmet : String, grupa : String):Unit{
            val lista = ArrayList<Kviz>()
            getAll().forEach {
                if (it.nazivPredmeta== predmet && it.nazivGrupe==grupa) {
                    if(!(Korisnik.kvizovi.contains(it))) {
                        lista.add(it)
                    }
                }
            }
            getMyKvizes().forEach { lista.add(it) }
            Korisnik.kvizovi= sortiraj(lista)
        }
    }
}