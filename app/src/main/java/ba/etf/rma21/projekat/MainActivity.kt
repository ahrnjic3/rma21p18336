package ba.etf.rma21.projekat

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ba.etf.rma21.projekat.data.models.Korisnik
import ba.etf.rma21.projekat.data.repositories.KvizRepository.Companion.dodajUMojeKvizove
import ba.etf.rma21.projekat.view.KvizListAdapter
import ba.etf.rma21.projekat.viewmodel.KvizListViewModel
import com.google.android.material.floatingactionbutton.FloatingActionButton


class MainActivity : AppCompatActivity() {
    private lateinit var  kvizovi: RecyclerView
    private lateinit var kvizoviAdapter: KvizListAdapter
    private var kvizListViewModel = KvizListViewModel();
    private lateinit var spinner: Spinner
    private lateinit var upisDugme : FloatingActionButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        kvizovi=findViewById(R.id.listaKvizova)
        kvizovi.layoutManager= GridLayoutManager(this, 2)
        kvizoviAdapter= KvizListAdapter(listOf())
        kvizovi.adapter = kvizoviAdapter
        kvizoviAdapter.updateKvizovi(kvizListViewModel.getAll())
        spinner=findViewById(R.id.filterKvizova)

        val naziviSpinn = resources.getStringArray(R.array.Spinner)
        val adapter = ArrayAdapter(this,android.R.layout.simple_spinner_item,naziviSpinn)
        spinner.adapter=adapter
        spinner.onItemSelectedListener= object: AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {

                kvizoviAdapter.updateKvizovi(kvizListViewModel.sortiraj(kvizListViewModel.getAll()))
                kvizoviAdapter.notifyDataSetChanged()

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

               /* if (spinner.selectedItem.toString()== naziviSpinn[0].toString()) kvizoviAdapter.updateKvizovi(kvizListViewModel.getMyKvizes())
                else  if (spinner.selectedItem.toString()== naziviSpinn[1].toString()) kvizoviAdapter.updateKvizovi(kvizListViewModel.getAll())
                else  if (spinner.selectedItem.toString()== naziviSpinn[2].toString()) kvizoviAdapter.updateKvizovi(kvizListViewModel.getDone())
                else  if (spinner.selectedItem.toString()== naziviSpinn[3].toString()) kvizoviAdapter.updateKvizovi(kvizListViewModel.getFuture())
                else  if (spinner.selectedItem.toString()== naziviSpinn[4].toString()) kvizoviAdapter.updateKvizovi(kvizListViewModel.getNotTaken())
                kvizoviAdapter.notifyDataSetChanged()*/
                provjeri()
            }
        }
        val REQUEST_CODE= 20;
        val intent = Intent(this, UpisPredmeta::class.java).apply{}
        upisDugme = findViewById(R.id.upisDugme)
        upisDugme.setOnClickListener {
            startActivityForResult(intent,REQUEST_CODE)
        }


    }

    override  fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == 20) {

            var Godina : String= data?.extras?.getString("godinaRez").toString()
            var Predmet : String= data?.extras?.getString("predmetRez").toString()
            var Grupa : String= data?.extras?.getString("grupaRez").toString()
            kvizListViewModel.dodajUMojeKvizove(Predmet,Grupa)
            Korisnik.kvizovi.forEach { println(it.nazivPredmeta + " Grupa : "+ it.nazivGrupe) }
            provjeri()
        }
    }
    fun provjeri (){
        val naziviSpinn = resources.getStringArray(R.array.Spinner)
        if (spinner.selectedItem.toString()== naziviSpinn[0].toString()) kvizoviAdapter.updateKvizovi(kvizListViewModel.getMyKvizes())
        else  if (spinner.selectedItem.toString()== naziviSpinn[1].toString()) kvizoviAdapter.updateKvizovi(kvizListViewModel.getAll())
        else  if (spinner.selectedItem.toString()== naziviSpinn[2].toString()) kvizoviAdapter.updateKvizovi(kvizListViewModel.getDone())
        else  if (spinner.selectedItem.toString()== naziviSpinn[3].toString()) kvizoviAdapter.updateKvizovi(kvizListViewModel.getFuture())
        else  if (spinner.selectedItem.toString()== naziviSpinn[4].toString()) kvizoviAdapter.updateKvizovi(kvizListViewModel.getNotTaken())
        kvizoviAdapter.notifyDataSetChanged()
    }

}

