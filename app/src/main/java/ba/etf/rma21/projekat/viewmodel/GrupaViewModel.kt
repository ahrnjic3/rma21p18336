package ba.etf.rma21.projekat.viewmodel

import ba.etf.rma21.projekat.data.models.Grupa
import ba.etf.rma21.projekat.data.repositories.GrupaRepository
import ba.etf.rma21.projekat.data.repositories.PredmetRepository
import ba.etf.rma21.projekat.data.repositories.grupe

class GrupaViewModel {

    fun getGroupsByPredmet(nazivPredmeta: String): List<Grupa> {
        // TODO: Implementirati
        return  GrupaRepository.getGroupsByPredmet(nazivPredmeta)
    }
}