package ba.etf.rma21.projekat.viewmodel

import ba.etf.rma21.projekat.data.models.Kviz
import ba.etf.rma21.projekat.data.repositories.*

class KvizListViewModel {

    fun getMyKvizes(): List<Kviz> {


        return KvizRepository.getMyKvizes()
    }

    fun getAll(): List<Kviz> {

        return KvizRepository.getAll()
    }

    fun getDone(): List<Kviz> {

        return KvizRepository.getDone()
    }

    fun getFuture(): List<Kviz> {

        return KvizRepository.getFuture()
    }

    fun getNotTaken(): List<Kviz> {

        return KvizRepository.getNotTaken()
    }
    fun sortiraj(kvizList : List<Kviz>):List<Kviz>{
        return  KvizRepository.sortiraj(kvizList)
    }
    fun filtriraj ( kvizList: List<Kviz>):List<Kviz>{
        return  KvizRepository.filtriraj(kvizList)
    }
    fun dodajUMojeKvizove(predmet : String, grupa : String):Unit{
        KvizRepository.dodajUMojeKvizove(predmet,grupa)
    }
}