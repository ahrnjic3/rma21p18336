package ba.etf.rma21.projekat.data.repositories

import ba.etf.rma21.projekat.data.models.Kviz
import java.util.*

fun kvizovi(): List<Kviz> {
    return listOf(
            Kviz("Kviz1","RMA", Date(121,8,1),Date(121,9,10),Date(121,9,10),2,"Grupa1",2.5f),

            Kviz("Kviz1","DM", Date(121,9,1),Date(121,10,10),Date(121,10,10),5,"Grupa1",2.5f),
            Kviz("Kviz2","DM", Date(121,9,1),Date(121,10,10),Date(121,10,10),5,"Grupa2",2.5f),

            Kviz("Kviz1","IM", Date(121,11,10),Date(121,11,15),null,5,"Grupa1",null),
            Kviz("Kviz2","IM", Date(121,11,10),Date(121,11,15),null,5,"Grupa2",null),

            Kviz("Kviz2","RMA",Date(121,2,10),Date(121,11,15),null,2,"Grupa2",null),
            Kviz("Kviz0","RMA", Date(121,2,10),Date(121,2,10),null,2,"Grupa2",null),

            Kviz("Kviz1","VI",Date(121,2,10),Date(121,11,15),null,2,"Grupa1",null),
            Kviz("Kviz2","VI", Date(121,9,1),Date(121,10,10),Date(121,10,10),5,"Grupa2",1.5f),

            Kviz("Kviz1","NASP",Date(121,2,10),Date(121,11,15),null,2,"Grupa1",null),
            Kviz("Kviz2","NASP", Date(121,9,1),Date(121,10,10),Date(121,10,10),5,"Grupa2",3.5f),

            Kviz("Kviz1","NOVI",Date(121,2,10),Date(121,11,15),null,2,"Grupa1",null),
            Kviz("Kviz2","NOVI", Date(121,9,1),Date(121,10,10),Date(121,10,10),5,"Grupa2",0.5f)


    )
}
fun mojiKvizovi(): List<Kviz>{
    return  listOf(
            Kviz("Kviz1","RMA", Date(121,8,1),Date(121,9,10),Date(121,9,10),2,"Grupa1",2.5f),
            Kviz("Kviz1","DM", Date(121,9,1),Date(121,10,10),Date(121,10,10),5,"Grupa1",2.5f),
            Kviz("Kviz1","IM", Date(121,11,10),Date(121,11,15),null,5,"Grupa1",null)
    )

}