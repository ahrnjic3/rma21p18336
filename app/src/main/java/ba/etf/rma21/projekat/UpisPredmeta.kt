package ba.etf.rma21.projekat

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.Spinner
import androidx.appcompat.app.AppCompatActivity
import ba.etf.rma21.projekat.viewmodel.GrupaViewModel
import ba.etf.rma21.projekat.viewmodel.PredmetViewModel


class UpisPredmeta : AppCompatActivity(){


    private lateinit var godinaSpinner: Spinner
    private lateinit var predmetSpinner: Spinner
    private lateinit var grupaSpinner : Spinner
    private lateinit var button: Button
    private var predmetViewModel = PredmetViewModel();
    private var grupaViewModel = GrupaViewModel();
    companion object{
        var god = 0;
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.upis_predmet)

        var buttonActive: Boolean;
        buttonActive = false;

        godinaSpinner = findViewById(R.id.odabirGodina)
        predmetSpinner = findViewById(R.id.odabirPredmet)
        grupaSpinner = findViewById(R.id.odabirGrupa)
        button = findViewById(R.id.dodajPredmetDugme)

        button.isClickable = false;

        val godine = resources.getStringArray(R.array.Godina)
        val adapterGodine = ArrayAdapter(this, android.R.layout.simple_spinner_item, godine)
        godinaSpinner.adapter = adapterGodine;

        /* val predmeti = predmetViewModel.dajPredmeteZaGodinu(godinaSpinner.selectedItem.toString().toInt()).toMutableList()
        val adapterPredmeti = ArrayAdapter(this,android.R.layout.simple_spinner_item,predmeti)
        predmetSpinner.adapter=adapterPredmeti


        val grupe = grupaViewModel.getGroupsByPredmet(predmetSpinner.selectedItem.toString())
        val adapterGrupe = ArrayAdapter(this,android.R.layout.simple_spinner_item,grupe)*/






        button.isEnabled = false
        button.isClickable = false
        val Context = godinaSpinner.context
        godinaSpinner.setSelection(god)
        godinaSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                val nazivi = ArrayList<String>()
                nazivi.add("")
                predmetSpinner.adapter = ArrayAdapter(Context, android.R.layout.simple_spinner_item, nazivi)
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                grupaSpinner.isEnabled = false
                predmetSpinner.isEnabled = false
                if (godinaSpinner.selectedItemPosition > 0) {
                    val predmeti = predmetViewModel.dajPredmeteZaGodinu(godinaSpinner.selectedItem.toString().toInt()).toMutableList()
                    val nazivi = ArrayList<String>()
                    nazivi.add("Odaberi predmet")
                    predmeti.forEach { nazivi.add(it.naziv) }
                    predmetSpinner.adapter = ArrayAdapter(Context, android.R.layout.simple_spinner_item, nazivi)
                    predmetSpinner.isEnabled = true;
                    god=godinaSpinner.selectedItemPosition.toInt()
                }

            }
        }

        predmetSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                val nazivi = ArrayList<String>()
                nazivi.add("Odaberi grupu")
                grupaSpinner.adapter = ArrayAdapter(Context, android.R.layout.simple_spinner_item, nazivi)

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                grupaSpinner.isEnabled = false;
                if (predmetSpinner.selectedItemPosition > 0) {
                    val grupe = grupaViewModel.getGroupsByPredmet(predmetSpinner.selectedItem.toString())
                    val nazivi = ArrayList<String>()
                    nazivi.add("Odaberi grupu")
                    grupe.forEach { nazivi.add(it.naziv) }
                    val adapterGrupe = ArrayAdapter(Context, android.R.layout.simple_spinner_item, nazivi)
                    grupaSpinner.adapter = adapterGrupe
                    grupaSpinner.isEnabled = true;
                }

            }
        }

        grupaSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                button.isEnabled = false
                button.isClickable = false

                if (grupaSpinner.selectedItemPosition > 0) {
                    button.isEnabled = true
                    button.isClickable = true
                }

            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }
        }


        button.setOnClickListener {

            val resultIntent: Intent = Intent()
            resultIntent.putExtra("godinaRez", godinaSpinner.selectedItem.toString())
            resultIntent.putExtra("predmetRez", predmetSpinner.selectedItem.toString())
            resultIntent.putExtra("grupaRez", grupaSpinner.selectedItem.toString())
            setResult(Activity.RESULT_OK, resultIntent)
            finish();
/*
            val intent = Intent(applicationContext, MainActivity::class.java)
            intent.putExtra("Predmet", predmetSpinner.selectedItem.toString())
            intent.putExtra("Gruap", grupaSpinner.selectedItem.toString())

            startActivity(intent)*/
        }
    }


}