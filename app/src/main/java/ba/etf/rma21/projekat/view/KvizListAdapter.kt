package ba.etf.rma21.projekat.view

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ba.etf.rma21.projekat.R
import ba.etf.rma21.projekat.data.models.Kviz
import java.text.SimpleDateFormat
import java.util.*

class KvizListAdapter(
        private var kvizes: List<Kviz>
) : RecyclerView.Adapter<KvizListAdapter.KvizViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): KvizViewHolder {
        val view = LayoutInflater
                .from(parent.context)
                .inflate(R.layout.kviz, parent, false)
        return KvizViewHolder(view)
    }
    override fun getItemCount(): Int = kvizes.size
    override fun onBindViewHolder(holder: KvizViewHolder, position: Int) {

        val pattern = "dd.MM.YYYY"
        val simpleDateFormat = SimpleDateFormat(pattern)
        val date = simpleDateFormat.format(kvizes[position].datumPocetka)
        holder.nazivPredemeta.text = kvizes[position].nazivPredmeta;
        holder.naziv.text= kvizes[position].naziv;
        holder.datum.text= date;
        holder.trajanje.text = kvizes[position].trajanje.toString()+" min"
        if(kvizes[position].osvojeniBodovi!=null) holder.bodovi.text= kvizes[position].osvojeniBodovi.toString()
        else  holder.bodovi.text=""
        holder.slika.setImageResource(R.drawable.zelena);


        val slika: String = bojaKruzic(kvizes[position])
        val context: Context = holder.slika.getContext()
        var id: Int = context.getResources()
                .getIdentifier(slika, "drawable", context.getPackageName())
        if (id===0) id=context.getResources()
                .getIdentifier("plava", "drawable", context.getPackageName())
        holder.slika.setImageResource(id)
    }
    fun updateKvizovi(kvizes: List<Kviz>) {
        this.kvizes = kvizes
        notifyDataSetChanged()
    }
    inner class KvizViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val nazivPredemeta: TextView = itemView.findViewById(R.id.nazivPredemeta)
        val naziv: TextView = itemView.findViewById(R.id.naziv)
        val datum: TextView = itemView.findViewById(R.id.Datum)
        val trajanje: TextView=itemView.findViewById(R.id.trajanje)
        val slika: ImageView = itemView.findViewById(R.id.slikaStanje)
        val bodovi: TextView = itemView.findViewById(R.id.bodovi)
    }
    fun  bojaKruzic(kviz : Kviz):String{
        if(kviz.osvojeniBodovi!= null) return "plava"
        if(Calendar.getInstance().time < kviz.datumPocetka) return "zuta"
        if(Calendar.getInstance().time > kviz.datumKraj) return  "crvena"
        if(kviz.datumPocetka<kviz.datumKraj && kviz.datumRada==null) return  "zelena"
        return " ";
    }

}