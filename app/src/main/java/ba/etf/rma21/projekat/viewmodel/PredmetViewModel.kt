package ba.etf.rma21.projekat.viewmodel

import ba.etf.rma21.projekat.data.models.Predmet
import ba.etf.rma21.projekat.data.repositories.PredmetRepository
import ba.etf.rma21.projekat.data.repositories.predmeti

class PredmetViewModel {
    fun getAll(): List<Predmet>{
        return  PredmetRepository.getAll();
    }
    fun dajPredmeteZaGodinu( god : Int):List<Predmet>{
        return PredmetRepository.dajPredmeteZaGodinu(god)
    }
}