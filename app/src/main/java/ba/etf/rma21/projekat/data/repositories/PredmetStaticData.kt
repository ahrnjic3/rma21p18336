package ba.etf.rma21.projekat.data.repositories

import ba.etf.rma21.projekat.data.models.Predmet

fun predmeti(): List<Predmet> {
    return listOf(
            Predmet("RMA",2),
            Predmet("DM",2),
            Predmet("IM",1),
            Predmet("VI",3),
            Predmet("NASP",4),
            Predmet("NOVI",5)

    )
}